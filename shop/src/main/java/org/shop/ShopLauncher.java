package org.shop;

import org.shop.Configuration.MainConfiguration;
import org.shop.api.ProductService;
import org.shop.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

/**
 * The ShopLauncher class.
 */

@Component

public class ShopLauncher {
    private DataInitializer dataInitializer;
    private ProductInitializer productInitializer;
    private ProposalInitializer proposalInitializer;
    private SellerInitializer sellerInitializer;
    private UserInitializer userInitializer;

    @Autowired
    public ShopLauncher(DataInitializer dataInitializer,
                        ProductInitializer productInitializer,
                        ProposalInitializer proposalInitializer,
                        SellerInitializer sellerInitializer,
                        UserInitializer userInitializer) {

        this.dataInitializer = dataInitializer;
        this.productInitializer = productInitializer;
        this.proposalInitializer = proposalInitializer;
        this.sellerInitializer = sellerInitializer;
        this.userInitializer = userInitializer;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfiguration.class);
        ShopLauncher shopLauncher =  context.getBean("shopLauncher", ShopLauncher.class);
        System.out.println(shopLauncher);

        UserService userService = (UserService) context.getBean("userService");
        userService.getUsers().forEach(user -> System.out.println(user));

        ProductService productService = (ProductService) context.getBean("productService");
        productService.getProducts().forEach(product -> System.out.println(product));

        System.out.println("Продукт Kindle Fire: " + (productService.getProductsByName("Kindle Fire")));
        System.out.println("Продукт id=3: " + productService.getProductById(3l));

    }

    @Override
    public String toString() {
        return "ShopLauncher{" +
                "dataInitializer=" + dataInitializer + '\n' +
                ", productInitializer=" + productInitializer + '\n' +
                ", proposalInitializer=" + proposalInitializer + '\n' +
                ", sellerInitializer=" + sellerInitializer + '\n' +
                ", userInitializer=" + userInitializer +
                '}';
    }
}

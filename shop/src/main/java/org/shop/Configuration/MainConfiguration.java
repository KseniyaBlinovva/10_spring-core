package org.shop.Configuration;

import org.shop.Configuration.util.DataInitializersConfiguration;
import org.shop.Configuration.util.FactoryConfiguration;
import org.shop.Configuration.util.RepositoryConfiguration;
import org.shop.Configuration.util.ServiseConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
@Configuration
@Import({DataInitializersConfiguration.class,
        FactoryConfiguration.class,
        RepositoryConfiguration.class,
        ServiseConfiguration.class})
public class MainConfiguration {
}

package org.shop.Configuration.util;

import org.shop.repository.UserRepository;
import org.shop.repository.factory.UserRepositoryFactory;
import org.shop.repository.map.OrderMapRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
public class RepositoryConfiguration {
    @Value("${intitialSequence}")
    long sequence;

    @Bean
    public UserRepositoryFactory repositoryFactory() {
        return new UserRepositoryFactory();
    }

    @Bean
    public UserRepository userRepository() {
        return repositoryFactory().createUserRepository();
    }

    @Bean
    public OrderMapRepository orderRepository() {
        OrderMapRepository orderMapRepository = new OrderMapRepository();
        orderMapRepository.setSequence(sequence);
        return orderMapRepository;
    }
}

package org.shop.Configuration.util;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "org.shop")
@PropertySource("application.properties")
public class DataInitializersConfiguration {

}
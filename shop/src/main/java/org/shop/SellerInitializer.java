package org.shop;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.shop.api.SellerService;
import org.shop.data.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * The Seller Initializer util class.
 */
@Component
public class SellerInitializer {
    /** The seller service. */

    @Autowired
    private SellerService sellerService;

    /** The seller names. */
    @Value("#{${sellerNames}}")
    private Map<Long, String> sellerNames;

    @Override
    public String toString() {
        return "SellerInitializer{" +
                "sellerService=" + sellerService +
                ", sellerNames=" + sellerNames +
                '}';
    }

    /**
     * Inits the sellers.
     */

    public void initSellers() {
        List<Seller> sellers = new LinkedList<Seller>();
        for (Map.Entry<Long, String> entry : sellerNames.entrySet()) {
            Seller seller = new Seller();
            seller.setId(entry.getKey());
            seller.setName(entry.getValue());

            sellers.add(seller);
        }

        sellerService.importSellers(sellers);
    }
}
